FROM node:boron

ARG BACKEND_HOST
ENV BACKEND_HOST=$BACKEND_HOST
ARG ENV
ENV ENV=$ENV

RUN mkdir -p /usr/src/app/microservice.football
RUN mkdir -p /usr/src/app/microservice.football/config
RUN mkdir -p /usr/src/app/microservice.football/javascript.feed-poller
RUN mkdir -p /usr/src/app/microservice.football/javascript.object-normalizer

WORKDIR /usr/src/app/microservice.football

COPY microservice.football/index.js /usr/src/app/microservice.football/index.js
COPY microservice.football/package.json /usr/src/app/microservice.football/package.json
COPY microservice.football/ConnectorService.js /usr/src/app/microservice.football/ConnectorService.js
COPY microservice.football/main.js /usr/src/app/microservice.football/main.js
COPY microservice.football/config/default.json /usr/src/app/microservice.football/config/default.json



COPY javascript.feed-poller/index.js /usr/src/app/microservice.football/javascript.feed-poller/index.js
COPY javascript.feed-poller/package.json /usr/src/app/microservice.football/javascript.feed-poller/package.json
COPY javascript.object-normalizer/index.js /usr/src/app/microservice.football/javascript.object-normalizer/index.js
COPY javascript.object-normalizer/package.json /usr/src/app/microservice.football/javascript.object-normalizer/package.json


RUN cd /usr/src/app/microservice.football/javascript.feed-poller/ && npm install
RUN cd /usr/src/app/microservice.football/javascript.object-normalizer/ && npm install

RUN npm install

CMD [ "npm", "start"]
